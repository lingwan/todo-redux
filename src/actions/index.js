import {ADD_TODO,TOGGLE_TODO,SET_FILTER,SET_TODO_TEXT} from "./actionTypes";

let nextTodoId=0


/**
 * add todo
 * @param {*} text 
 */
export const addTodo=(text) => ({
    type:ADD_TODO,
    id:nextTodoId++,
    text
})
console.log(nextTodoId)
/**
 * change the state of todo
 * @param {*} id 
 */
export const toggleTodo=id => ({
    type:TOGGLE_TODO,
    id
})

/**
 * set the state of filter
 * @param {*} filter 
 */
export const setFilter=filter => ({
    type:SET_FILTER,
    filter
})

/**
 * set the text of new todo
 * @param {*} text 
 */
export const setTodoText=text => ({
    type:SET_TODO_TEXT,
    text
})